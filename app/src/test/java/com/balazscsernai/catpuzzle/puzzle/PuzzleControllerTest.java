package com.balazscsernai.catpuzzle.puzzle;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Balázs on 2015.05.28..
 */
public class PuzzleControllerTest extends TestCase {

    PuzzleController puzzleController;
    List<PuzzlePiece> puzzlePieces;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testSize() throws Exception {
        // Given
        final int size = 3;
        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getSize()).andReturn(size).once();
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        int retSize = puzzleController.getSize();

        // Then
        Assert.assertEquals(String.format("Puzzle size should be %s", size), size, retSize);
    }

    @Test
    public void testCount() {
        // Given
        final int count = 9;
        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getCount()).andReturn(count).once();
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        int retCount = puzzleController.getCount();

        //Then
        Assert.assertEquals(String.format("Puzzle piece count should be %d", count), count, retCount);
    }

    @Test
    public void testRow() {
        // Given
        final int size = 2;
        final int count = size * size;
        final int[] rows = new int[]{0, 0, 1, 1};
        List<PuzzlePiece> mockPuzzlePieces = new ArrayList<>(count);
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                PuzzlePiece mockPuzzlePiece = EasyMock.createMock(PuzzlePiece.class);
                EasyMock.expect(mockPuzzlePiece.getOriginalRow()).andReturn(row).once();
                EasyMock.replay(mockPuzzlePiece);
                mockPuzzlePieces.add(mockPuzzlePiece);
            }
        }
        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getPieces()).andReturn(mockPuzzlePieces).times(count);
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        int[] retRows = new int[count];
        for (int index = 0; index < count; index++) {
            retRows[index] = puzzleController.getRow(index);
        }

        // Then
        for (int index = 0; index < count; index++) {
            Assert.assertEquals(String.format("Puzzle piece %d row should be %d", index, rows[index]), rows[index], retRows[index]);
        }
    }

    @Test
    public void testColumn() {
        // Given
        final int size = 2;
        final int count = size * size;
        final int[] cols = new int[]{0, 1, 0, 1};
        List<PuzzlePiece> mockPuzzlePieces = new ArrayList<>(count);
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                PuzzlePiece mockPuzzlePiece = EasyMock.createMock(PuzzlePiece.class);
                EasyMock.expect(mockPuzzlePiece.getOriginalColumn()).andReturn(col).once();
                EasyMock.replay(mockPuzzlePiece);
                mockPuzzlePieces.add(mockPuzzlePiece);
            }
        }
        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getPieces()).andReturn(mockPuzzlePieces).times(count);
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        int[] retCols = new int[count];
        for (int index = 0; index < count; index++) {
            retCols[index] = puzzleController.getColumn(index);
        }

        // Then
        for (int index = 0; index < count; index++) {
            Assert.assertEquals(String.format("Puzzle piece %d column should be %d", index, cols[index]), cols[index], retCols[index]);
        }
    }

    @Test
    public void testFinished() {
        // Given
        final int size = 3;
        final int count = size * size;
        List<PuzzlePiece> mockPuzzlePieces = new ArrayList<>(count);
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                PuzzlePiece mockPuzzlePiece = EasyMock.createMock(PuzzlePiece.class);
                EasyMock.expect(mockPuzzlePiece.getCurrentRow()).andReturn(row).once();
                EasyMock.expect(mockPuzzlePiece.getOriginalRow()).andReturn(row).once();
                EasyMock.expect(mockPuzzlePiece.getCurrentColumn()).andReturn(col).once();
                EasyMock.expect(mockPuzzlePiece.getOriginalColumn()).andReturn(col).once();
                EasyMock.replay(mockPuzzlePiece);
                mockPuzzlePieces.add(mockPuzzlePiece);
            }
        }
        Puzzle mockPuzzle = EasyMock.createStrictMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getPieces()).andReturn(mockPuzzlePieces).once();
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        boolean retFinished = puzzleController.isFinished();

        // Then
        Assert.assertEquals("Puzzle piece are not scrambled, so puzzle should be finihsed", true, retFinished);
    }

    /**
     * Swapping two puzzle pieces.
     */
    @Test
    public void testSuccessfulSwap() {
        // Given
        final int size = 2;
        final int oneRow = 2;
        final int oneCol = 3;
        final int twoRow = 2;
        final int twoCol = 4;

        List<PuzzlePiece> mockPuzzlePieces = new ArrayList<>(size);

        PuzzlePiece mockPuzzlePieceOne = EasyMock.createMock(PuzzlePiece.class);
        EasyMock.expect(mockPuzzlePieceOne.getCurrentRow()).andReturn(oneRow).times(2);
        EasyMock.expect(mockPuzzlePieceOne.getCurrentColumn()).andReturn(oneCol).times(2);
        mockPuzzlePieceOne.setCurrentRow(EasyMock.eq(twoRow));
        EasyMock.expectLastCall().once();
        mockPuzzlePieceOne.setCurrentColumn(EasyMock.eq(twoCol));
        EasyMock.expectLastCall().once();
        EasyMock.replay(mockPuzzlePieceOne);
        mockPuzzlePieces.add(mockPuzzlePieceOne);

        PuzzlePiece mockPuzzlePieceTwo = EasyMock.createMock(PuzzlePiece.class);
        EasyMock.expect(mockPuzzlePieceTwo.getCurrentRow()).andReturn(twoRow).times(2);
        EasyMock.expect(mockPuzzlePieceTwo.getCurrentColumn()).andReturn(twoCol).times(2);
        mockPuzzlePieceTwo.setCurrentRow(EasyMock.eq(oneRow));
        EasyMock.expectLastCall().once();
        mockPuzzlePieceTwo.setCurrentColumn(EasyMock.eq(oneCol));
        EasyMock.expectLastCall().once();
        EasyMock.replay(mockPuzzlePieceTwo);
        mockPuzzlePieces.add(mockPuzzlePieceTwo);

        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getPieces()).andReturn(mockPuzzlePieces).times(size);
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        boolean retSwap = puzzleController.swap(0, 1);

        // Then
        EasyMock.verify(mockPuzzle, mockPuzzlePieceOne, mockPuzzlePieceTwo);
        Assert.assertEquals(true, retSwap);
    }

    @Test
    public void testFailedSwap() {
        // Given
        final int size = 2;
        final int oneRow = 2;
        final int oneCol = 1;
        final int twoRow = 2;
        final int twoCol = 3;

        List<PuzzlePiece> mockPuzzlePieces = new ArrayList<>(size);

        PuzzlePiece mockPuzzlePieceOne = EasyMock.createMock(PuzzlePiece.class);
        EasyMock.expect(mockPuzzlePieceOne.getCurrentRow()).andReturn(oneRow).once();
        EasyMock.expect(mockPuzzlePieceOne.getCurrentColumn()).andReturn(oneCol).once();
        EasyMock.replay(mockPuzzlePieceOne);
        mockPuzzlePieces.add(mockPuzzlePieceOne);

        PuzzlePiece mockPuzzlePieceTwo = EasyMock.createMock(PuzzlePiece.class);
        EasyMock.expect(mockPuzzlePieceTwo.getCurrentRow()).andReturn(twoRow).once();
        EasyMock.expect(mockPuzzlePieceTwo.getCurrentColumn()).andReturn(twoCol).once();
        EasyMock.replay(mockPuzzlePieceTwo);
        mockPuzzlePieces.add(mockPuzzlePieceTwo);

        Puzzle mockPuzzle = EasyMock.createNiceMock(Puzzle.class);
        EasyMock.expect(mockPuzzle.getPieces()).andReturn(mockPuzzlePieces).times(size);
        EasyMock.replay(mockPuzzle);
        puzzleController = new PuzzleController(mockPuzzle);

        // When
        boolean retSwap = puzzleController.swap(0, 1);

        // Then
        EasyMock.verify(mockPuzzle, mockPuzzlePieceOne, mockPuzzlePieceTwo);
        Assert.assertEquals(false, retSwap);
    }
}

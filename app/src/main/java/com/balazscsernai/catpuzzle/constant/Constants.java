package com.balazscsernai.catpuzzle.constant;

/**
 * Application wide used constatns.
 * Created by Balazs_Csernai on 5/26/2015.
 */
public class Constants {

    public static final int PUZZLE_SIZE_MIN = 2;
    public static final int PUZZLE_SIZE_DEF = 3;
    public static final int PUZZLE_SIZE_MAX = 8;

    public static final int CAT_API_MAX_RESULTS_PER_PAGE = 100;
    public static final int CAT_API_MIN_RESULTS_PER_PAGE = 1;
    public static final int CAT_API_DEF_RESULTS_PER_PAGE = 1;
    public static final int MIN_IMAGE_INDEX = 0;

    public static final String EXTRA_IMAGE_INDEX = "extra.imageIndex";
    public static final String EXTRA_IMAGES = "extra.images";
    public static final String EXTRA_IMAGE_ID = "extra.imageId";
    public static final String EXTRA_PUZZLE = "extra.puzzle";
}

package com.balazscsernai.catpuzzle.puzzle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by Balázs on 2015.05.28..
 */
public class PuzzlePieceFactoryImpl implements PuzzlePieceFactory {

    /**
     * Injectable constructor.
     */
    @Inject
    public PuzzlePieceFactoryImpl() {
    }

    @Override
    public List<PuzzlePiece> create(int size) {
        List<PuzzlePiece> puzzlePieces = new ArrayList(size * size);
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                puzzlePieces.add(new PuzzlePiece(row, col, size));
            }
        }
        return puzzlePieces;
    }
}

package com.balazscsernai.catpuzzle.puzzle;

/**
 * Creator for puzzle game space model.
 * Created by Balazs_Csernai on 5/26/2015.
 */
public interface PuzzleFactory {

    /**
     * Create puzzle game space model.
     * @param size Puzzle size
     * @return Puzzle game space model
     */
    Puzzle create(int size);
}

package com.balazscsernai.catpuzzle.puzzle;

import java.util.List;

import javax.inject.Inject;

/**
 * Creator for puzzle pieces.
 * Created by Balázs on 2015.05.28..
 */
public interface PuzzlePieceFactory {

    /**
     * Creates puzzle pieces for a 2d dimensional (size x size) puzzle.
     * @param size Size of the puzzle
     * @return Puzzle pieces
     */
    List<PuzzlePiece> create(int size);
}

package com.balazscsernai.catpuzzle.puzzle;

import java.io.Serializable;

/**
 * Model for a puzzle piece.
 * Created by Balázs on 2015.05.25..
 */
public class PuzzlePiece implements Serializable {

    private final int puzzleSize;
    private final int originalRow;
    private final int originalColumn;
    private int currentRow;
    private int currentColumn;

    /**
     * Constructor.
     * @param row Row index
     * @param column Column index
     * @param puzzleSize Size of the puzzle
     */
    PuzzlePiece(int row, int column, int puzzleSize) {
        this.puzzleSize = puzzleSize;
        originalRow = row;
        originalColumn = column;
        currentRow = row;
        currentColumn = column;
    }

    /**
     * Returns the original row index.
     * @return Row index
     */
    int getOriginalRow() {
        return originalRow;
    }

    /**
     * Returns the original column index.
     * @return Column index
     */
    int getOriginalColumn() {
        return originalColumn;
    }

    /**
     * Returns the current row index.
     * @return Row index
     */
    int getCurrentRow() {
        return currentRow;
    }

    /**
     * Sets the current row index.
     * @param currentRow Row index
     */
    void setCurrentRow(int currentRow) {
        this.currentRow = currentRow;
    }

    /**
     * Returns the current column index
     * @return Column index
     */
    int getCurrentColumn() {
        return currentColumn;
    }

    /**
     * Sets the current column index
     * @param currentColumn Column index
     */
    void setCurrentColumn(int currentColumn) {
        this.currentColumn = currentColumn;
    }

    @Override
    public int hashCode() {
        return originalRow * puzzleSize + originalColumn;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) return true;
        if (o != null && o instanceof PuzzlePiece) {
            return hashCode() == o.hashCode();
        }
        return false;
    }

    @Override
    public String toString() {
        return new StringBuilder().append("{or=").append(originalRow)
                .append(" oc=").append(originalColumn)
                .append(" cr=").append(currentRow)
                .append(" cc=").append(currentColumn)
                .append("}").toString();

    }
}

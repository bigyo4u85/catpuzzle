package com.balazscsernai.catpuzzle.puzzle;

import java.util.ArrayList;
import java.util.List;

/**
 * Model for a 2 dimensional puzzle game space.
 * <em>Methods are made package private to avoid messing with the game space without its controller.</em>
 * Created by Balázs on 2015.05.25..
 */
public class Puzzle {

    private List<PuzzlePiece> puzzlePieces;
    private int puzzleSize;

    /**
     * Constructor.
     */
    Puzzle() {
        puzzlePieces = new ArrayList<>();
    }

    /**
     * Returns the size of the puzzle.
     * @return Puzzle size
     */
    int getSize() {
        return puzzleSize;
    }

    /**
     * Sets the size of the puzzle.
     * @param size Puzzle size
     */
    void setSize(int size) {
        puzzleSize = size;
    }

    /**
     * Returns the number of puzzle pieces.
     * @return Number of puzzle pieces
     */
    int getCount() {
        return puzzleSize * puzzleSize;
    }

    /**
     * Returns the puzzle pieces.
     * @return Puzzle pieces
     */
    List<PuzzlePiece> getPieces() {
        return new ArrayList<PuzzlePiece>(puzzlePieces);
    }

    /**
     * Sets the puzzle pieces.
     * @param puzzlePieces Puzzle pieces
     */
    void setPieces(List<PuzzlePiece> puzzlePieces) {
        this.puzzlePieces.clear();
        this.puzzlePieces.addAll(puzzlePieces);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder().append("[");
        for (int index = 0; index < puzzlePieces.size(); index++) {
            if (index > 0) {
                sb.append(" ");
            }
            sb.append(puzzlePieces.get(index).toString());
        }
        return sb.append("]").toString();
    }
}

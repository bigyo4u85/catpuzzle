package com.balazscsernai.catpuzzle.puzzle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Controller for puzzle game space.
 * Created by Balázs on 2015.05.27..
 */
public class PuzzleController implements Serializable {

    private static final String TAG = PuzzleController.class.getSimpleName();

    private final Puzzle puzzle;
    private int moves;
    private int time;

    /**
     * Constructor.
     *
     * @param puzzle Puzzle game space.
     */
    PuzzleController(Puzzle puzzle) {
        this.puzzle = puzzle;
    }

    /**
     * Scrambles the puzzle.
     */
    public void scramble() {
        while (isFinished()) {
            List<PuzzlePiece> puzzlePieces = puzzle.getPieces();
            List<PuzzlePiece> scrambled = new ArrayList<PuzzlePiece>(puzzlePieces.size());
            Random random = new Random();
            int puzzleSize = puzzle.getSize();
            int puzzleCount = puzzle.getCount();
            for (int index = puzzleCount; index > 0; index--) {
                PuzzlePiece piece = puzzlePieces.remove(random.nextInt(index));
                piece.setCurrentRow((puzzleCount - index) / puzzleSize);
                piece.setCurrentColumn((puzzleCount - index) % puzzleSize);
                scrambled.add(piece);
            }
            puzzle.setPieces(scrambled);
        }
    }

    /**
     * Returns the size of the puzzle.
     *
     * @return Puzzle size
     */
    public int getSize() {
        return puzzle.getSize();
    }

    /**
     * Returns the number of puzzle pieces.
     *
     * @return Number of puzzle pieces
     */
    public int getCount() {
        return puzzle.getCount();
    }

    /**
     * Returns the row number of the puzzle pieces before scrambling.
     *
     * @param index Index of the puzzle piece
     * @return Row number or -1;
     */
    public int getRow(int index) {
        PuzzlePiece puzzlePiece = puzzle.getPieces().get(index);
        return puzzlePiece == null ? -1 : puzzlePiece.getOriginalRow();
    }

    /**
     * Returns the column number of the puzzle piece before scrambling.
     *
     * @param index Index of the puzzle piece
     * @return Column number
     */
    public int getColumn(int index) {
        PuzzlePiece puzzlePiece = puzzle.getPieces().get(index);
        return puzzlePiece == null ? -1 : puzzlePiece.getOriginalColumn();
    }

    /**
     * Checks if all the puzzle pieces are in their place.
     *
     * @return True if all the puzzle places are in there place
     */
    public boolean isFinished() {
        for (PuzzlePiece puzzlePiece : puzzle.getPieces()) {
            if (puzzlePiece.getCurrentRow() != puzzlePiece.getOriginalRow() ||
                    puzzlePiece.getCurrentColumn() != puzzlePiece.getOriginalColumn()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Swaps two puzzle pieces if they are neighbouring.
     *
     * @param pieceOne Index of a puzzle piece
     * @param pieceTwo Index of another puzzle piece
     * @return True if puzzle pieces are swapped
     */
    public boolean swap(int pieceOne, int pieceTwo) {
        PuzzlePiece one = puzzle.getPieces().get(pieceOne);
        PuzzlePiece two = puzzle.getPieces().get(pieceTwo);
        if (one != null && two != null && getPieceDistance(one, two) == 1) {
            int oneRow = one.getCurrentRow();
            int oneCol = one.getCurrentColumn();
            one.setCurrentRow(two.getCurrentRow());
            one.setCurrentColumn(two.getCurrentColumn());
            two.setCurrentRow(oneRow);
            two.setCurrentColumn(oneCol);
            return true;
        }
        return false;
    }

    /**
     * Returns the number of moves.
     *
     * @return Moves
     */
    public int getMoves() {
        return moves;
    }

    /**
     * Sets the number of moves.
     *
     * @param moves Moves
     */
    public void setMoves(int moves) {
        this.moves = moves;
    }

    /**
     * Returns the number of seconds.
     *
     * @return Seconds
     */
    public int getTimeSeconds() {
        return time;
    }

    /**
     * Sets the number of seconds.
     *
     * @param time Seconds
     */
    public void setTimeSeconds(int time) {
        this.time = time;
    }

    /**
     * Returns the distance of two puzzle pieces.
     *
     * @param one A puzzle piece
     * @param two Another puzzle piece
     * @return Distance
     */
    private int getPieceDistance(PuzzlePiece one, PuzzlePiece two) {
        return Math.abs(one.getCurrentRow() - two.getCurrentRow()) + Math.abs(one.getCurrentColumn() - two.getCurrentColumn());
    }
}

package com.balazscsernai.catpuzzle.puzzle;

import com.balazscsernai.catpuzzle.constant.Constants;

import javax.inject.Inject;

/**
 * Created by Balázs on 2015.05.25..
 */
public class PuzzleFactoryImpl implements PuzzleFactory {

    private final PuzzlePieceFactory puzzlePieceFactory;

    @Inject
    public PuzzleFactoryImpl(PuzzlePieceFactory puzzlePieceFactory) {
        this.puzzlePieceFactory = puzzlePieceFactory;
    }

    @Override
    public Puzzle create(int size) {
        Puzzle puzzle = new Puzzle();
        if (size >= Constants.PUZZLE_SIZE_MIN && size <= Constants.PUZZLE_SIZE_MAX) {
            puzzle.setSize(size);
            puzzle.setPieces(puzzlePieceFactory.create(size));
        } else {
            puzzle.setSize(Constants.PUZZLE_SIZE_DEF);
            puzzle.setPieces(puzzlePieceFactory.create(Constants.PUZZLE_SIZE_DEF));
        }
        return puzzle;
    }
}

package com.balazscsernai.catpuzzle.puzzle;

/**
 * Creator for puzzle controller.
 * Created by Balázs on 2015.05.27..
 */
public interface PuzzleControllerFactory {

    /**
     * Creates puzzle controller.
     * @param size Size of the puzzle
     * @return Controller
     */
    PuzzleController create(int size);
}

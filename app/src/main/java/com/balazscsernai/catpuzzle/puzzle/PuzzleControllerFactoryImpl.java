package com.balazscsernai.catpuzzle.puzzle;

import javax.inject.Inject;

/**
 * Creator for puzzle controller.
 * Created by Balázs on 2015.05.27..
 */
public class PuzzleControllerFactoryImpl implements PuzzleControllerFactory {

    private final PuzzleFactory puzzleFactory;

    /**
     * Injectable constructor.
     *
     * @param puzzleFactory Puzzle factory
     */
    @Inject
    public PuzzleControllerFactoryImpl(PuzzleFactory puzzleFactory) {
        this.puzzleFactory = puzzleFactory;
    }

    @Override
    public PuzzleController create(int size) {
        return new PuzzleController(puzzleFactory.create(size));
    }
}

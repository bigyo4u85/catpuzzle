package com.balazscsernai.catpuzzle.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.balazscsernai.catpuzzle.constant.Constants;
import com.balazscsernai.catpuzzle.screen.puzzle.PuzzleActivity;

/**
 * Intent related utility.
 * Created by Balazs_Csernai on 5/26/2015.
 */
public class IntentUtil {

    private static final String EXTRA_PUZZLE_SIZE = "extra.puzzleSize";
    private static final String EXTRA_BITMAP = "extra.bitmap";

    /**
     * Private constructor for utility.
     */
    private IntentUtil() {

    }

    /**
     * Creates and intent for starting puzzle.
     * @param context Android context
     * @param puzzleSize Size of the puzzle
     * @param imageId ID of the puzzle image
     * @return Intent
     */
    public static Intent createStartPuzzle(Context context, int puzzleSize, String imageId) {
        return new Intent(context, PuzzleActivity.class).putExtra(EXTRA_PUZZLE_SIZE, puzzleSize).putExtra(EXTRA_BITMAP, imageId);
    }

    /**
     * Extracts the size of the puzzle from and intent.
     * @param intent Intent
     * @return Size of the puzzle
     */
    public static int getPuzzleSizeExtra(Intent intent) {
        return intent.getIntExtra(EXTRA_PUZZLE_SIZE, Constants.PUZZLE_SIZE_DEF);
    }

    /**
     * Extracts the ID of the puzzle image.
     * @param intent Intent
     * @return ID of the puzzle image
     */
    public static String getBitmapExtra(Intent intent) {
        return intent.getStringExtra(EXTRA_BITMAP);
    }
}

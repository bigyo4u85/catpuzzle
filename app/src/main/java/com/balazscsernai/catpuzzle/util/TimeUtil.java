package com.balazscsernai.catpuzzle.util;

/**
 * Created by Balázs on 2015.05.26..
 */
public class TimeUtil {

    private static final int HOURS = 60 * 60;
    private static final int MINUTES = 60;

    /**
     * Private constructor for utility.
     */
    private TimeUtil() {
    }

    /**
     * Converts to elapsed seconds to text.
     * @param seconds Elapsed seconds
     * @return Text
     */
    public static final String elapsedTime(int seconds) {
        int elapsed = seconds;
        StringBuilder sb = new StringBuilder();
        if (elapsed / HOURS > 0) {
            sb.append(elapsed / HOURS).append(":");
            elapsed %= HOURS;
        }
        sb.append(String.format("%02d", elapsed / MINUTES)).append(":");
        elapsed %= MINUTES;
        sb.append(String.format("%02d", elapsed));
        return sb.toString();
    }
}

package com.balazscsernai.catpuzzle.image;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;

/**
 * Helper for decoding bitmaps received through network.
 * <em>Algorithms are very similar to the ones used in Picasso because THEY ARE copied from Picasso.</em>
 * Created by Balázs on 2015.05.24..
 */
public class BitmapDecoder {

    private final Context context;

    /**
     * Injectable constructor.
     * @param context
     */
    @Inject
    public BitmapDecoder(Context context) {
        this.context = context;
    }

    /**
     * Decodes a bitmap respecting the desired width and height requirements.
     * <em>Using sample sizing (2^n), so only very big or very small images are resized.</em>
     * @param input Byte stream
     * @param widthResId Width dimension requirement
     * @param heightResId Height dimension requirement
     * @return Decoded bitmap
     * @throws IOException Network error
     */
    public Bitmap decode(InputStream input, int widthResId, int heightResId) throws IOException {
        BitmapFactory.Options options = createBitmapOptions();
        int reqWidth = context.getResources().getDimensionPixelSize(widthResId);
        int reqHeight = context.getResources().getDimensionPixelSize(heightResId);
        byte[] bytes = toByteArray(input);

        BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        calculateInSampleSize(widthResId, heightResId, options.outWidth, options.outHeight, options);
        return transform(reqWidth, reqHeight, BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options));
    }

    /**
     * Creates an initial bitmap configuration for reading just the size of the bitmap.
     * @return Bitmap configuration
     */
    private BitmapFactory.Options createBitmapOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        return options;
    }

    /**
     * Returns the byte from input stream to a byte array.
     * @param input Byte stream
     * @return Byte array
     * @throws IOException Network error
     */
    private byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int n;
        while ((n = input.read(buffer)) != -1) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }

    /**
     * Calculates sampling size.
     * @param reqWidth Width requirement
     * @param reqHeight Height requirement
     * @param width Original width
     * @param height Original height
     * @param options Bitmap configuration
     */
    private void calculateInSampleSize(int reqWidth, int reqHeight, int width, int height, BitmapFactory.Options options) {
        int sampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int heightRatio;
            int widthRatio;
            if (reqHeight == 0) {
                sampleSize = (int) Math.floor((float) width / (float) reqWidth);
            } else if (reqWidth == 0) {
                sampleSize = (int) Math.floor((float) height / (float) reqHeight);
            } else {
                heightRatio = (int) Math.floor((float) height / (float) reqHeight);
                widthRatio = (int) Math.floor((float) width / (float) reqWidth);
                sampleSize = Math.max(heightRatio, widthRatio);
            }
        }
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;
    }

    /**
     * Resizes to bitmap according to the required width and height.
     * @param reqWidth Width requirement
     * @param reqHeight Height requirement
     * @param bitmap Bitmap
     * @return Resized bitmap
     */
    private Bitmap transform(int reqWidth, int reqHeight, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        int inWidth = bitmap.getWidth();
        int inHeight = bitmap.getHeight();
        int drawX = 0;
        int drawY = 0;
        int drawWidth = inWidth;
        int drawHeight = inHeight;
        float widthRatio = reqWidth / (float) inWidth;
        float heightRatio = reqHeight / (float) inHeight;
        float scaleX, scaleY;

        if (widthRatio > heightRatio) {
            int newSize = (int) Math.ceil(inHeight * (heightRatio / widthRatio));
            drawY = (inHeight - newSize) / 2;
            drawHeight = newSize;
            scaleX = widthRatio;
            scaleY = reqHeight / (float) drawHeight;
        } else {
            int newSize = (int) Math.ceil(inWidth * (widthRatio / heightRatio));
            drawX = (inWidth - newSize) / 2;
            drawWidth = newSize;
            scaleX = reqWidth / (float) drawWidth;
            scaleY = heightRatio;
        }
        if (inWidth > reqWidth || inHeight > reqHeight) {
            matrix.preScale(scaleX, scaleY);
        }

        Bitmap newBitmap = Bitmap.createBitmap(bitmap, drawX, drawY, drawWidth, drawHeight, matrix, true);

        if (newBitmap != bitmap) {
            bitmap.recycle();
            bitmap = newBitmap;
        }

        return bitmap;
    }
}

package com.balazscsernai.catpuzzle.screen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.balazscsernai.catpuzzle.R;
import com.balazscsernai.catpuzzle.util.TimeUtil;

import javax.inject.Inject;

import retrofit.RetrofitError;

/**
 * Created by Balazs_Csernai on 5/28/2015.
 */
public class DialogFactoryImpl implements DialogFactory {

    @Inject
    public DialogFactoryImpl() {
    }

    @Override
    public AlertDialog createImageDownloadFailed(Context context, String message) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.errorDialog_title))
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.errorDialog_acknowledge), new ErrorAcknowledgeListener())
                .create();
    }

    @Override
    public AlertDialog createGiveUp(Context context, int moves, int seconds, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.puzzleDialog_giveUpTitle))
                .setMessage(context.getString(R.string.puzzleDialog_giveUpMessage).replace("[TIME]", TimeUtil.elapsedTime(seconds)).replace("[MOVE]", Integer.valueOf(moves).toString()))
                .setNegativeButton(context.getString(R.string.puzzleDialog_giveUpNegative), negativeListener)
                .setPositiveButton(context.getString(R.string.puzzleDialog_giveUpPositive), positiveListener)
                .create();
    }

    @Override
    public AlertDialog createFinished(Context context, int moves, int seconds, DialogInterface.OnClickListener positiveListener) {
        return new AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.puzzleDialog_finishedTitle))
                .setMessage(context.getString(R.string.puzzleDialog_finishedMessage).replace("[TIME]", TimeUtil.elapsedTime(seconds)).replace("[MOVE]", Integer.valueOf(moves).toString()))
                .setPositiveButton(context.getString(R.string.puzzleDialog_finishedPositive), positiveListener)
                .create();
    }

    private final class ErrorAcknowledgeListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }
}

package com.balazscsernai.catpuzzle.screen;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Creator for dialogs.
 * Created by Balazs_Csernai on 5/28/2015.
 */
public interface DialogFactory {

    /**
     * Creates dialog for image download failure.
     *
     * @param context Android context
     * @param message Error message
     * @return Alert dialog
     */
    AlertDialog createImageDownloadFailed(Context context, String message);

    /**
     * Creates dialog for giving up acknowledgement.
     *
     * @param context          Android context
     * @param moves            Moves
     * @param seconds          Elapsed seconds
     * @param positiveListener Positive dialog listener
     * @param negativeListener Negative dialog listener
     * @return Alert dialog
     */
    AlertDialog createGiveUp(Context context, int moves, int seconds, DialogInterface.OnClickListener positiveListener, DialogInterface.OnClickListener negativeListener);

    /**
     * Creates dialog for finishing puzzle.
     *
     * @param context          Android context
     * @param moves            Moves
     * @param seconds          Elapsed seconds
     * @param positiveListener Positive dialog listener
     * @return Alert dialog
     */
    AlertDialog createFinished(Context context, int moves, int seconds, DialogInterface.OnClickListener positiveListener);

}

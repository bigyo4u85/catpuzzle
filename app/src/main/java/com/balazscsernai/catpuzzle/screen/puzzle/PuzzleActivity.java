package com.balazscsernai.catpuzzle.screen.puzzle;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;

import com.balazscsernai.catpuzzle.CatPuzzleApplication;
import com.balazscsernai.catpuzzle.R;
import com.balazscsernai.catpuzzle.constant.Constants;
import com.balazscsernai.catpuzzle.network.CatAPIClient;
import com.balazscsernai.catpuzzle.puzzle.PuzzleController;
import com.balazscsernai.catpuzzle.puzzle.PuzzleControllerFactory;
import com.balazscsernai.catpuzzle.screen.DialogFactory;
import com.balazscsernai.catpuzzle.util.IntentUtil;
import com.balazscsernai.catpuzzle.util.TimeUtil;
import com.balazscsernai.catpuzzle.view.PuzzleView;
import com.balazscsernai.catpuzzle.view.PuzzleViewController;
import com.balazscsernai.catpuzzle.view.PuzzleViewWidget;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Balazs_Csernai on 5/26/2015.
 */
public class PuzzleActivity extends ActionBarActivity implements PuzzleViewController.PuzzlePieceMoveListener {

    @Inject
    CatAPIClient apiClient;
    @Inject
    DialogFactory dialogFactory;
    @Inject
    PuzzleViewController puzzleViewController;
    @Inject
    PuzzleControllerFactory puzzleControllerFactory;
    @InjectView(R.id.puzzleActivity_puzzle)
    PuzzleViewWidget puzzleView;
    @InjectView(R.id.puzzleActivity_header)
    GridLayout headerLayout;
    @InjectView(R.id.puzzleActivity_moves)
    TextView movesTextView;
    @InjectView(R.id.puzzleActivity_time)
    TextView timeTextView;
    @InjectView(R.id.puzzleActivity_giveUp)
    Button giveUpButton;
    private PuzzleController puzzleController;
    private int puzzleSize;
    private String imageId;
    private Bitmap bitmap;
    private AlertDialog dialog;
    private boolean timeIsTickingAway;
    private Timer gameTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((CatPuzzleApplication) getApplication()).inject(this);
        setContentView(R.layout.activity_puzzle);
        ButterKnife.inject(this);
        puzzleSize = IntentUtil.getPuzzleSizeExtra(getIntent());
        imageId = IntentUtil.getBitmapExtra(getIntent());
        gameTimer = new Timer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startGame();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopGame();
    }

    @Override
    public void onBackPressed() {
        onGiveUpClick();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(Constants.EXTRA_IMAGE_ID) && savedInstanceState.containsKey(Constants.EXTRA_PUZZLE)) {
            imageId = savedInstanceState.getString(Constants.EXTRA_IMAGE_ID);
            puzzleController = (PuzzleController) savedInstanceState.getSerializable(Constants.EXTRA_PUZZLE);
            startGame();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(Constants.EXTRA_IMAGE_ID, imageId);
        outState.putSerializable(Constants.EXTRA_PUZZLE, puzzleController);
    }

    @OnClick(R.id.puzzleActivity_giveUp)
    void onGiveUpClick() {
        stopGameTimer();
        dialog = dialogFactory.createGiveUp(PuzzleActivity.this, puzzleController.getMoves(), puzzleController.getTimeSeconds(), new GiveUpAcknowledgementListener(), new GiveUpCancellationListener());
        dialog.show();
    }

    @Override
    public void onPieceMoved() {
        puzzleController.setMoves(puzzleController.getMoves() + 1);
        movesTextView.setText(Integer.valueOf(puzzleController.getMoves()).toString());
        if (puzzleController.isFinished()) {
            stopGame();
            dialog = dialogFactory.createFinished(PuzzleActivity.this, puzzleController.getMoves(), puzzleController.getTimeSeconds(), new FinishedAcknowledgementListener());
            dialog.show();
        }
    }

    private void downloadImage(String imageId) {
        apiClient.getImage(imageId, new ImageDownloadCallback());
    }

    private void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    private void startGame() {
        if (bitmap == null && imageId != null) {
            downloadImage(imageId);
        } else if (bitmap != null && puzzleController == null) {
            createPuzzle();
            startGame();
        } else if (puzzleController != null) {
            setMovesAndTime(puzzleController.getMoves(), puzzleController.getTimeSeconds());
            startGameTimer();
        }
    }

    private void createPuzzle() {
        puzzleController = puzzleControllerFactory.create(puzzleSize);
        puzzleController.scramble();
        puzzleController.setMoves(0);
        puzzleController.setTimeSeconds(0);
        puzzleViewController.setView(puzzleView)
                .setBitmap(bitmap)
                .setPuzzleController(puzzleController)
                .setMoveListener(PuzzleActivity.this);
    }

    private void setMovesAndTime(int moves, int time) {
        movesTextView.setText(Integer.valueOf(moves).toString());
        timeTextView.setText(TimeUtil.elapsedTime(time));
    }

    private void startGameTimer() {
        if (!timeIsTickingAway) {
            timeIsTickingAway = true;
            gameTimer = new Timer();
            gameTimer.schedule(new TimeUpdater(), 1000l, 1000l);
        }
    }

    private void stopGame() {
        stopGameTimer();
    }

    private void stopGameTimer() {
        if (timeIsTickingAway) {
            timeIsTickingAway = false;
            gameTimer.cancel();
        }
    }

    private final class ImageDownloadCallback implements Callback<Bitmap> {

        @Override
        public void success(Bitmap bitmap, Response response) {
            setBitmap(bitmap);
            startGame();
        }

        @Override
        public void failure(RetrofitError error) {
            dialog = dialogFactory.createImageDownloadFailed(PuzzleActivity.this, error.getMessage());
            dialog.show();
        }
    }

    private final class GiveUpAcknowledgementListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            finish();
        }
    }

    private final class GiveUpCancellationListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            startGame();
        }
    }

    private final class FinishedAcknowledgementListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            finish();
        }
    }

    private final class TimeUpdater extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    puzzleController.setTimeSeconds(puzzleController.getTimeSeconds() + 1);
                    timeTextView.setText(TimeUtil.elapsedTime(puzzleController.getTimeSeconds()));
                }
            });
        }
    }
}

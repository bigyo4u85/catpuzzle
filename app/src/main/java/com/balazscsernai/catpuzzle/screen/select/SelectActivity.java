package com.balazscsernai.catpuzzle.screen.select;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.balazscsernai.catpuzzle.CatPuzzleApplication;
import com.balazscsernai.catpuzzle.R;
import com.balazscsernai.catpuzzle.constant.Constants;
import com.balazscsernai.catpuzzle.network.CatAPIClient;
import com.balazscsernai.catpuzzle.network.CatAPIImages;
import com.balazscsernai.catpuzzle.screen.DialogFactory;
import com.balazscsernai.catpuzzle.util.IntentUtil;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class SelectActivity extends ActionBarActivity {

    @Inject
    CatAPIClient apiClient;
    @Inject
    DialogFactory dialogFactory;
    @InjectView(R.id.mainActivity_downloadIndicator)
    ProgressBar downloadIndicator;
    @InjectView(R.id.mainActivity_downloadFailed)
    TextView downloadFailedTextView;
    @InjectView(R.id.mainActivity_puzzleImage)
    ImageView puzzleImage;
    @InjectView(R.id.mainActivity_puzzleSizeSpinner)
    Spinner puzzleSizeSpinner;
    @InjectView(R.id.mainActivity_getImageButton)
    Button getImageButton;
    @InjectView(R.id.mainActivity_startButton)
    Button startButton;

    private AlertDialog dialog;
    private CatAPIImages images;
    private int imageIndex;
    private int puzzleSize = Constants.PUZZLE_SIZE_DEF;
    private boolean downloading;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((CatPuzzleApplication) getApplication()).inject(this);
        setContentView(R.layout.activity_select);
        ButterKnife.inject(this);
        initViews();
        downloadImages();
    }

    private void initViews() {
        puzzleSizeSpinner.setAdapter(new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.puzzle_sizes)));
        puzzleSizeSpinner.setOnItemSelectedListener(new PuzzleSizeSelectionListener());
    }

    @Override
    protected void onResume() {
        super.onResume();
        downloadImages();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey(Constants.EXTRA_IMAGE_INDEX) && savedInstanceState.containsKey(Constants.EXTRA_IMAGES)) {
            imageIndex = savedInstanceState.getInt(Constants.EXTRA_IMAGE_INDEX);
            images = (CatAPIImages) savedInstanceState.getSerializable(Constants.EXTRA_IMAGES);
        }
        downloadImage(images.getImage(imageIndex).getUrl());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(Constants.EXTRA_IMAGE_INDEX, imageIndex);
        outState.putSerializable(Constants.EXTRA_IMAGES, images);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (dialog != null) {
            dialog.dismiss();
        }
        downloading = false;
    }

    @OnClick(R.id.mainActivity_getImageButton)
    void onGetImageButtonClick() {
        downloadImage(images.getImage(nextImageIndex()).getUrl());
    }

    private int nextImageIndex() {
        imageIndex += 1;
        if (imageIndex >= Math.max(Constants.CAT_API_MAX_RESULTS_PER_PAGE, images.size())) {
            imageIndex = Constants.MIN_IMAGE_INDEX;
        }
        return imageIndex;
    }

    @OnClick(R.id.mainActivity_startButton)
    void onStartButtonClick() {
        if (images != null) {
            startActivity(IntentUtil.createStartPuzzle(SelectActivity.this, puzzleSize, images.getImage(imageIndex).getId()));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    private void downloadImages() {
        if (!downloading && images == null) {
            downloading = true;
            apiClient.getImageList(Constants.CAT_API_MAX_RESULTS_PER_PAGE, new ImageListDownloadCallback());
        }
    }

    private void downloadImage(String imageUrl) {
        if (!downloading) {
            downloading = true;
            downloadIndicator.setVisibility(View.VISIBLE);
            downloadFailedTextView.setVisibility(View.INVISIBLE);
            Picasso.with(SelectActivity.this)
                    .load(imageUrl)
                    .centerCrop()
                    .resizeDimen(R.dimen.puzzle_width, R.dimen.puzzle_height)
                    .into(puzzleImage, new ImageDownloadCallback());
        }
    }

    private final class ImageListDownloadCallback implements Callback<CatAPIImages> {

        @Override
        public void success(CatAPIImages catAPIImages, Response response) {
            downloading = false;
            imageIndex = 0;
            images = catAPIImages;
            downloadImage(images.getImage(imageIndex).getUrl());
        }

        @Override
        public void failure(RetrofitError error) {
            dialog = dialogFactory.createImageDownloadFailed(SelectActivity.this, error.getMessage());
            dialog.show();
        }
    }

    private class ImageDownloadCallback implements com.squareup.picasso.Callback {
        @Override
        public void onSuccess() {
            downloading = false;
            downloadIndicator.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onError() {
            downloading = false;
            downloadIndicator.setVisibility(View.INVISIBLE);
            downloadFailedTextView.setVisibility(View.VISIBLE);
        }
    }

    private final class PuzzleSizeSelectionListener implements AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            puzzleSize = position + 2;
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}

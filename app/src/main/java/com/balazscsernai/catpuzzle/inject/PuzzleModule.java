package com.balazscsernai.catpuzzle.inject;

import com.balazscsernai.catpuzzle.puzzle.PuzzleControllerFactory;
import com.balazscsernai.catpuzzle.puzzle.PuzzleControllerFactoryImpl;
import com.balazscsernai.catpuzzle.puzzle.PuzzleFactory;
import com.balazscsernai.catpuzzle.puzzle.PuzzleFactoryImpl;
import com.balazscsernai.catpuzzle.puzzle.PuzzlePieceFactory;
import com.balazscsernai.catpuzzle.puzzle.PuzzlePieceFactoryImpl;
import com.balazscsernai.catpuzzle.view.PuzzlePieceViewFactory;
import com.balazscsernai.catpuzzle.view.PuzzlePieceViewFactoryImpl;
import com.balazscsernai.catpuzzle.view.PuzzleViewController;
import com.balazscsernai.catpuzzle.view.PuzzleViewControllerImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Module for injecting puzzle related stuff.
 * Created by Balázs on 2015.05.24..
 */
@Module(library = true, includes = AndroidModule.class)
public class PuzzleModule {

    @Provides
    PuzzlePieceViewFactory providePuzzlePieceFactory(PuzzlePieceViewFactoryImpl puzzlePieceFactory) {
        return puzzlePieceFactory;
    }

    @Provides
    PuzzleViewController providePuzzleViewController(PuzzleViewControllerImpl viewController) {
        return viewController;
    }

    @Provides
    PuzzleFactory providePuzzleFactory(PuzzleFactoryImpl puzzleFactory) {
        return puzzleFactory;
    }

    @Provides
    PuzzleControllerFactory providePuzzleControllerFactory(PuzzleControllerFactoryImpl puzzleControllerFactory) {
        return puzzleControllerFactory;
    }

    @Provides
    PuzzlePieceFactory providePuzzlePieceFactory(PuzzlePieceFactoryImpl puzzlePieceFactory) {
        return puzzlePieceFactory;
    }
}

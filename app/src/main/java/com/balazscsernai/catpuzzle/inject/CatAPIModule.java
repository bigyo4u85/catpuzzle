package com.balazscsernai.catpuzzle.inject;

import com.balazscsernai.catpuzzle.network.BITMAP;
import com.balazscsernai.catpuzzle.network.CatAPI;
import com.balazscsernai.catpuzzle.network.CatAPIBitmapResponseParser;
import com.balazscsernai.catpuzzle.network.CatAPIClient;
import com.balazscsernai.catpuzzle.network.CatAPIClientImpl;
import com.balazscsernai.catpuzzle.network.CatAPICustomConverter;
import com.balazscsernai.catpuzzle.network.CatAPIHTMLResponseParser;
import com.balazscsernai.catpuzzle.network.CatAPIImpl;
import com.balazscsernai.catpuzzle.network.CatAPIJsonResponseParser;
import com.balazscsernai.catpuzzle.network.CatAPIQueryMapFactory;
import com.balazscsernai.catpuzzle.network.CatAPIQueryMapFactoryImpl;
import com.balazscsernai.catpuzzle.network.CatAPIResponseParser;
import com.balazscsernai.catpuzzle.network.HTML;
import com.balazscsernai.catpuzzle.network.JSON;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.converter.Converter;

/**
 * Module for injection Cat API related stuff.
 * Created by Balazs_Csernai on 5/26/2015.
 */
@Module(library = true, includes = AndroidModule.class)
public class CatAPIModule {
    @Provides
    @Singleton
    CatAPI provideCatAPI(CatAPIImpl catAPI) {
        return catAPI;
    }

    @Provides
    @Singleton
    CatAPIQueryMapFactory provideCatAPIQueryMapFactory(CatAPIQueryMapFactoryImpl queryMapFactory) {
        return queryMapFactory;
    }

    @Singleton
    @Provides
    CatAPIClient provideCatAPIClient(CatAPIClientImpl apiClient) {
        return apiClient;
    }

    @Provides
    @HTML
    CatAPIResponseParser provideCatAPIResponseParser(CatAPIHTMLResponseParser responseParser) {
        return responseParser;
    }

    @Provides
    @BITMAP
    CatAPIResponseParser provideCatAPIResponseParser(CatAPIBitmapResponseParser responseParser) {
        return responseParser;
    }

    @Provides
    @JSON
    CatAPIResponseParser provideCatAPIResponseParser(CatAPIJsonResponseParser responseParser) {
        return responseParser;
    }

    @Provides
    Converter provideConverter(CatAPICustomConverter converter) {
        return converter;
    }

}

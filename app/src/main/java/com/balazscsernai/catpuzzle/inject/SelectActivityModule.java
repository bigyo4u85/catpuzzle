package com.balazscsernai.catpuzzle.inject;

import com.balazscsernai.catpuzzle.screen.DialogFactory;
import com.balazscsernai.catpuzzle.screen.DialogFactoryImpl;
import com.balazscsernai.catpuzzle.screen.select.SelectActivity;

import dagger.Module;
import dagger.Provides;

/**
 * Module for injecting an Activity.
 * Created by Balazs_Csernai on 5/26/2015.
 */
@Module(injects = SelectActivity.class, includes = {AndroidModule.class, CatAPIModule.class})
public class SelectActivityModule {
}

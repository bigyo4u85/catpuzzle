package com.balazscsernai.catpuzzle.inject;

import android.content.Context;

import com.balazscsernai.catpuzzle.CatPuzzleApplication;
import com.balazscsernai.catpuzzle.screen.DialogFactory;
import com.balazscsernai.catpuzzle.screen.DialogFactoryImpl;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;

/**
 * Module for injecting general and Android related stuff
 * Created by Balazs_Csernai on 5/26/2015.
 */
@Module(library = true)
public class AndroidModule {

    private final CatPuzzleApplication application;

    public AndroidModule(CatPuzzleApplication application) {
        this.application = application;
    }

    @Provides
    Gson provideGson() {
        return new Gson();
    }

    @Provides
    Context provideContext() {
        return application.getApplicationContext();
    }

    @Provides
    DialogFactory provideDialogFactory(DialogFactoryImpl dialogFactory) {
        return dialogFactory;
    }
}

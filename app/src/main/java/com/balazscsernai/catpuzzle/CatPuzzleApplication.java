package com.balazscsernai.catpuzzle;

import android.app.Application;

import com.balazscsernai.catpuzzle.inject.AndroidModule;
import com.balazscsernai.catpuzzle.inject.CatAPIModule;
import com.balazscsernai.catpuzzle.inject.PuzzleActivityModule;
import com.balazscsernai.catpuzzle.inject.PuzzleModule;
import com.balazscsernai.catpuzzle.inject.SelectActivityModule;
import com.balazscsernai.catpuzzle.screen.puzzle.PuzzleActivity;

import dagger.ObjectGraph;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatPuzzleApplication extends Application {

    private ObjectGraph graph;

    @Override
    public void onCreate() {
        super.onCreate();
        setupDagger();
    }

    private void setupDagger() {
        graph = ObjectGraph.create(new Object[] {
                new AndroidModule(this),
                new PuzzleModule(),
                new CatAPIModule(),
                new SelectActivityModule(),
                new PuzzleActivityModule()
        });
    }

    public void inject(Object object) {
        graph.inject(object);
    }
}

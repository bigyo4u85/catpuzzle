package com.balazscsernai.catpuzzle.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.RelativeLayout;

import com.balazscsernai.catpuzzle.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Balázs on 2015.05.25..
 */
public class PuzzleViewWidget extends RelativeLayout implements PuzzleView {
    private static final String TAG = PuzzleViewWidget.class.getSimpleName();

    @InjectView(R.id.puzzle)
    GridLayout puzzleLayout;
    private List<PuzzlePieceView> pieces;
    private PieceClickListener pieceClickListener;
    private PieceSwapAnimation animation;
    private PieceSwapAnimation.AnimationListener animationListener;

    public PuzzleViewWidget(Context context) {
        this(context, null);
    }

    public PuzzleViewWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PuzzleViewWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.view_puzzle, this, true);
        if (!isInEditMode()) {
            ButterKnife.inject(this, view);
        }
    }

    @Override
    public void setPieces(int size, List<PuzzlePieceView> pieces) {
        this.pieces = pieces;
        int pieceWidth = puzzleLayout.getWidth() / size;
        int pieceHeight = puzzleLayout.getHeight() / size;
        puzzleLayout.setColumnCount(size);
        puzzleLayout.setRowCount(size);
        ViewGroup.LayoutParams pieceLayoutParams = new ViewGroup.LayoutParams(pieceWidth, pieceHeight);
        for (PuzzlePieceView view : pieces) {
            view.setOnClickListener(new InnerPuzzlePieceClickListener());
            view.setLayoutParams(pieceLayoutParams);
            puzzleLayout.addView(view);
        }
    }

    @Override
    public void setAnimationListener(PieceSwapAnimation.AnimationListener animationListener) {
        this.animationListener = animationListener;
    }

    @Override
    public void clear() {
        puzzleLayout.removeAllViews();
    }

    @Override
    public void setSelected(int pieceIndex, boolean selected) {
        pieces.get(pieceIndex).setSelected(selected);
    }

    @Override
    public void swap(int pieceOne, int pieceTwo) {
        animation = new PieceSwapAnimation(pieces.get(pieceOne), pieces.get(pieceTwo));
        animation.setAnimationListener(animationListener);
        animation.start();
    }

    @Override
    public void setPieceClickListener(PieceClickListener pieceClickListener) {
        this.pieceClickListener = pieceClickListener;

    }

    @Override
    public void setVisibility(boolean visible) {
        this.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public boolean isVisible() {
        return getVisibility() == View.VISIBLE;
    }

    private final class InnerPuzzlePieceClickListener implements OnClickListener {
        @Override
        public void onClick(View view) {
            if (pieceClickListener != null) {
                pieceClickListener.onPieceClick(((Integer) view.getTag()).intValue());
            }
        }
    }

    public static interface PieceClickListener {
        void onPieceClick(int pieceIndex);
    }
}
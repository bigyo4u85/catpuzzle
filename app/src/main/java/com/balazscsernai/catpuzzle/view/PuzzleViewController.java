package com.balazscsernai.catpuzzle.view;

import android.graphics.Bitmap;

import com.balazscsernai.catpuzzle.puzzle.PuzzleController;

/**
 * Puzzle view controller.
 * Created by Balazs_Csernai on 5/26/2015.
 */
public interface PuzzleViewController extends PuzzleViewWidget.PieceClickListener, PieceSwapAnimation.AnimationListener {
    /**
     * Sets the puzzle view.
     *
     * @param view Puzzle view
     * @return This controller
     */
    PuzzleViewController setView(PuzzleView view);

    /**
     * Sets the puzzle bitmap.
     *
     * @param bitmap Bitmap
     * @return This controller
     */
    PuzzleViewController setBitmap(Bitmap bitmap);

    /**
     * Sets the puzzle controller.
     *
     * @param puzzleController Puzzle controller
     */
    PuzzleViewController setPuzzleController(PuzzleController puzzleController);

    /**
     * Returns the puzzle controller.
     *
     * @return Puzzle controller
     */
    PuzzleController getPuzzleController();

    /**
     * Sets the listener for puzzle piece movement.
     *
     * @param moveListener Listener
     */
    PuzzleViewController setMoveListener(PuzzlePieceMoveListener moveListener);

    public static interface PuzzlePieceMoveListener {
        void onPieceMoved();
    }
}

package com.balazscsernai.catpuzzle.view;

import java.util.List;

/**
 * View model for a 2 dimensional puzzle.
 * Created by Balazs_Csernai on 5/28/2015.
 */
public interface PuzzleView {
    /**
     * Sets the puzzle piece views.
     * @param size Size of the puzzle
     * @param pieces Puzzle piece view
     */
    void setPieces(int size, List<PuzzlePieceView> pieces);

    /**
     * Sets the swap animation listener.
     * @param animationListener Swap animation listener
     */
    void setAnimationListener(PieceSwapAnimation.AnimationListener animationListener);

    /**
     * Removes the puzzle piece views.
     */
    void clear();

    /**
     * Sets a puzzle piece view selected.
     * @param pieceIndex Puzzle piece index
     * @param selected True to set selected
     */
    void setSelected(int pieceIndex, boolean selected);

    /**
     * Swaps two puzzle piece views.
     * @param pieceOne Puzzle piece view
     * @param pieceTwo Puzzle piece view
     */
    void swap(int pieceOne, int pieceTwo);

    /**
     * Sets the click listener for puzzle piece view.
     * @param pieceClickListener Click listener
     */
    void setPieceClickListener(PuzzleViewWidget.PieceClickListener pieceClickListener);

    /**
     * Sets the visibility of the puzzle
     * @param visible True to set visible
     */
    void setVisibility(boolean visible);

    /**
     * Checks if the puzzle is visible.
     * @return True if visible
     */
    boolean isVisible();
}

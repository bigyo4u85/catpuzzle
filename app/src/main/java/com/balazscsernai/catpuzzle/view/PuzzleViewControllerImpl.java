package com.balazscsernai.catpuzzle.view;

import android.graphics.Bitmap;

import com.balazscsernai.catpuzzle.puzzle.PuzzleController;

import javax.inject.Inject;

/**
 * Puzzle view controller.
 * Created by Balázs on 2015.05.25..
 */
public class PuzzleViewControllerImpl implements PuzzleViewController {

    private PuzzleView view;
    private PuzzlePieceViewFactory factory;
    private PuzzleController puzzleController;
    private int selected;
    private Bitmap bitmap;
    private PuzzlePieceMoveListener moveListener;
    private boolean animating;

    /**
     * Injectable constructor.
     *
     * @param factory Puzzle piece view factory
     */
    @Inject
    public PuzzleViewControllerImpl(PuzzlePieceViewFactory factory) {
        this.factory = factory;
        selected = -1;
    }

    @Override
    public PuzzleViewController setView(PuzzleView view) {
        this.view = view;
        view.setPieceClickListener(this);
        view.setAnimationListener(this);
        return this;
    }

    @Override
    public PuzzleViewController setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        return this;
    }

    @Override
    public PuzzleViewController setPuzzleController(PuzzleController puzzleController) {
        this.puzzleController = puzzleController;
        view.clear();
        view.setPieces(puzzleController.getSize(), factory.createPuzzlePieceViews(bitmap, puzzleController));
        view.setVisibility(true);
        return this;
    }

    @Override
    public PuzzleController getPuzzleController() {
        return puzzleController;
    }

    @Override
    public PuzzleViewController setMoveListener(PuzzlePieceMoveListener moveListener) {
        this.moveListener = moveListener;
        return this;
    }

    @Override
    public void onPieceClick(int pieceIndex) {
        if (!animating) {
            if (selected == -1) {
                selected = pieceIndex;
                view.setSelected(pieceIndex, true);
            } else if (selected == pieceIndex) {
                selected = -1;
                view.setSelected(pieceIndex, false);
            } else {
                if (puzzleController.swap(selected, pieceIndex)) {
                    view.setSelected(selected, false);
                    view.swap(selected, pieceIndex);
                    selected = -1;
                }
            }
        }
    }

    @Override
    public void onAnimationStarted() {
        animating = true;
    }

    @Override
    public void onAnimationFinished() {
        animating = false;
        if (moveListener != null) {
            moveListener.onPieceMoved();
        }
    }
}

package com.balazscsernai.catpuzzle.view;

import android.graphics.Bitmap;

import com.balazscsernai.catpuzzle.puzzle.PuzzleController;

import java.util.List;

/**
 * Puzzle piece view creator.
 * Created by Balazs_Csernai on 5/26/2015.
 */
public interface PuzzlePieceViewFactory {

    /**
     * Crates puzzle piece views by slicing the provided bitmap.
     * @param bitmap Bitmap
     * @param puzzleController Puzzle controller
     * @return Puzzle piece views
     */
    List<PuzzlePieceView> createPuzzlePieceViews(Bitmap bitmap, PuzzleController puzzleController);
}

package com.balazscsernai.catpuzzle.view;

import android.content.Context;
import android.graphics.Bitmap;

import com.balazscsernai.catpuzzle.puzzle.PuzzleController;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Puzzle piece view creator.
 * Created by Balázs on 2015.05.25..
 */
public class PuzzlePieceViewFactoryImpl implements PuzzlePieceViewFactory {

    private final Context context;

    /**
     * Injectable constructor.
     * @param context Android context
     */
    @Inject
    public PuzzlePieceViewFactoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public List<PuzzlePieceView> createPuzzlePieceViews(Bitmap bitmap, PuzzleController puzzleController) {
        final int width = (int) Math.floor(bitmap.getWidth() / (float) puzzleController.getSize());
        final int height = (int) Math.floor(bitmap.getHeight() / (float) puzzleController.getSize());
        List<PuzzlePieceView> views = new ArrayList<PuzzlePieceView>(puzzleController.getCount());

        for (int index = 0; index < puzzleController.getCount(); index++) {
            PuzzlePieceView view = new PuzzlePieceView(context);
            view.setImageBitmap(Bitmap.createBitmap(bitmap, puzzleController.getColumn(index) * width, puzzleController.getRow(index) * height, width, height));
            view.setTag(Integer.valueOf(index));
            views.add(view);
        }

        return views;
    }
}

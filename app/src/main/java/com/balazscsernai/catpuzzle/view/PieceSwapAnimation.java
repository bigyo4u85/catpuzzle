package com.balazscsernai.catpuzzle.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;

/**
 * Animation for swapping puzzle pieces.
 * Created by Balázs on 2015.05.25..
 */
public class PieceSwapAnimation implements Animator.AnimatorListener {

    private AnimatorSet animation;
    private AnimationListener animationListener;

    /**
     * Constructor.
     * @param one Puzzle piece
     * @param two Puzzle piece
     */
    public PieceSwapAnimation(PuzzlePieceView one, PuzzlePieceView two) {
        animation = new AnimatorSet();
        animation.play(ObjectAnimator.ofFloat(one, "x", one.getX(), two.getX()))
                .with(ObjectAnimator.ofFloat(one, "y", one.getY(), two.getY()))
                .with(ObjectAnimator.ofFloat(two, "x", two.getX(), one.getX()))
                .with(ObjectAnimator.ofFloat(two, "y", two.getY(), one.getY()));
        animation.setDuration(250l);
        animation.addListener(this);
    }

    /**
     * Sets the animation listener.
     * @param animationListener Animation listener.
     */
    public void setAnimationListener(AnimationListener animationListener) {
        this.animationListener = animationListener;
    }

    /**
     * Starts the animation.
     */
    public void start() {
        animation.start();
    }

    /**
     * Cancels the animation.
     */
    public void cancel() {
        animation.cancel();
    }

    @Override
    public void onAnimationStart(Animator animation) {
        if (animationListener != null) {
            animationListener.onAnimationStarted();
        }
    }

    @Override
    public void onAnimationEnd(Animator animation) {
        if (animationListener != null) {
            animationListener.onAnimationFinished();
        }
    }

    @Override
    public void onAnimationCancel(Animator animation) {
        // Not needed.
    }

    @Override
    public void onAnimationRepeat(Animator animation) {
        // Not needed.
    }

    public static interface AnimationListener {
        void onAnimationStarted();
        void onAnimationFinished();
    }
}

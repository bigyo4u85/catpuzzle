package com.balazscsernai.catpuzzle.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.balazscsernai.catpuzzle.R;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * View for puzzle piece.
 * Created by Balázs on 2015.05.25..
 */
public class PuzzlePieceView extends RelativeLayout {

    @InjectView(R.id.puzzlePiece)
    ImageView imageView;

    /**
     * Constructor.
     *
     * @param context Android context
     */
    public PuzzlePieceView(Context context) {
        this(context, null);
    }

    /**
     * Constructor.
     *
     * @param context Android context
     * @param attrs   Attribute set
     */
    public PuzzlePieceView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Constructor.
     *
     * @param context      Android context
     * @param attrs        Attribute set
     * @param defStyleAttr Default style
     */
    public PuzzlePieceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View view = LayoutInflater.from(context).inflate(R.layout.view_puzzle_piece, this, true);
        if (!isInEditMode()) {
            ButterKnife.inject(this, view);
        }
    }

    /**
     * Sets the bitmap of the puzzle piece.
     *
     * @param bitmap Bitmap
     */
    public void setImageBitmap(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void setSelected(boolean selected) {
        super.setSelected(selected);
        setBackgroundResource(selected ? R.color.puzzle_piece_selected_background : R.color.puzzle_piece_background);
    }
}

package com.balazscsernai.catpuzzle.network;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import retrofit.mime.TypedInput;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIHTMLResponseParser implements CatAPIResponseParser<CatAPIImages> {

    private static final String TAG = CatAPIHTMLResponseParser.class.getSimpleName();
    private static final Pattern HTML_HREF_ATTRIBUTE = Pattern.compile("href=\"[^\"]+\"");
    private static final Pattern ID_PARAM = Pattern.compile("id=[^\"]+\"");
    private static final int ID_LENGTH = "id=".length();
    private static final Pattern HTML_SRC_ATTRIBUTE = Pattern.compile("src=\"[^\"]+\"");
    private static final int SRC_LENGTH = "src=\"".length();

    @Inject
    public CatAPIHTMLResponseParser() {
    }

    @Override
    public CatAPIImages parse(TypedInput body, Type type) {
        StringBuilder parsed = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(body.in()));
            String line;
            while ((line = br.readLine()) != null) {
                parsed.append(line);
            }
            br.close();
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }
        return parse(parsed.toString());
    }

    private CatAPIImages parse(String response) {
        CatAPIImages images = new CatAPIImages();

        // Find image href
        Matcher hrefMather = HTML_HREF_ATTRIBUTE.matcher(response);
        while (hrefMather.find()) {
            int hrefEnd = hrefMather.end();
            String href = response.substring(hrefMather.start(), hrefEnd);
            // Find image id
            Matcher idMatcher = ID_PARAM.matcher(href);
            if (idMatcher.find()) {
                images.addImage(new CatAPIImage().setId(href.substring(idMatcher.start() + ID_LENGTH, idMatcher.end() - 1)));
            }
        }

        // Find image src
        Matcher srcMatcher = HTML_SRC_ATTRIBUTE.matcher(response);
        int imageIndex = 0;
        int numberOfImages = images.size();
        while (srcMatcher.find() && imageIndex < numberOfImages) {
            images.getImage(imageIndex).setUrl(response.substring(srcMatcher.start() + SRC_LENGTH, srcMatcher.end() - 1));
            imageIndex++;
        }

        return images;
    }
}

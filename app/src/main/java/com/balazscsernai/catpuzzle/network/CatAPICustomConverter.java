package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;

import com.google.gson.Gson;

import java.lang.reflect.Type;

import javax.inject.Inject;

import retrofit.converter.ConversionException;
import retrofit.converter.Converter;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPICustomConverter implements Converter {
    private static final String TAG = CatAPICustomConverter.class.getSimpleName();

    private final CatAPIResponseParser htmlResponseParser;
    private final CatAPIResponseParser bitmapResponseParser;
    private final CatAPIResponseParser jsonResponseParser;

    @Inject
    public CatAPICustomConverter(Gson gson,
                                 @HTML CatAPIResponseParser htmlResponseParser,
                                 @BITMAP CatAPIResponseParser bitmapResponseParser,
                                 @JSON CatAPIResponseParser jsonResponseParser) {
        this.htmlResponseParser = htmlResponseParser;
        this.bitmapResponseParser = bitmapResponseParser;
        this.jsonResponseParser = jsonResponseParser;
    }

    @Override
    public Object fromBody(TypedInput body, Type type) throws ConversionException {
        if (CatAPIImages.class.equals(type)) {
            return htmlResponseParser.parse(body, type);
        } else if (Bitmap.class.equals(type)) {
            return bitmapResponseParser.parse(body, type);
        }
        return jsonResponseParser.parse(body, type);
    }

    @Override
    public TypedOutput toBody(Object object) {
        return null;
    }
}

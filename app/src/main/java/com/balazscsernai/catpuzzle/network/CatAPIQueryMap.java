package com.balazscsernai.catpuzzle.network;

import com.balazscsernai.catpuzzle.constant.Constants;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Query map for CatAPI.
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIQueryMap {

    private final Map<String, String> query;

    /**
     * Private constructor.
     * Can be created only by builder.
     */
    private CatAPIQueryMap() {
        query = new HashMap<String, String>();
    }

    /**
     * Adds a parameter to the query map.
     * @param paramName Name of the parameter
     * @param paramValue Value of the parameter
     */
    private void addParam(String paramName, String paramValue) {
        query.put(paramName, paramValue);
    }

    /**
     * Returns the query map.
     * @return Query map.
     */
    public Map<String, String> getQueryMap() {
        return Collections.unmodifiableMap(query);
    }

    /**
     * Interface for setting the format of the image(s).
     */
    public static interface ImageFormatSetter {
        /**
         * Sets the format of the image(s).
         *
         * @param imageFormat CatAPI compatible image format
         * @return Image(s) per page setter
         */
        ImageResultsPerPageSetter format(CatAPIImageFormat imageFormat);
    }

    /**
     * Interface for setting a specific image.
     */
    public static interface ImageIdSetter {
        /**
         * Sets a specific image by id.
         * @param id Image ID
         * @return Final query builder
         */
        ImageSizeSetter id(String id);
    }

    /**
     * Query map builder.
     */
    public static final class Builder implements ImageFormatSetter, ImageIdSetter {

        private final CatAPIQueryMap query;

        /**
         * Constructor.
         */
        public Builder() {
            query = new CatAPIQueryMap();
        }

        @Override
        public ImageResultsPerPageSetter format(CatAPIImageFormat imageFormat) {
            query.addParam("format", imageFormat.name());
            return new ImageResultsPerPageSetterImpl(query);
        }

        @Override
        public ImageSizeSetter id(String id) {
            query.addParam("id", id);
            return new ImageSizeSetterImpl(query);
        }
    }

    /**
     * Interface for setting the number of image(s) per page.
     */
    public static interface ImageResultsPerPageSetter {
        /**
         * Sets the number of image(s) per page.
         *
         * @param resultsPerPage CatAPI compatible number.
         * @return Image type setter.
         */
        ImageSizeSetter resultsPerPage(int resultsPerPage);
    }

    private static final class ImageResultsPerPageSetterImpl implements ImageResultsPerPageSetter {

        private final CatAPIQueryMap query;

        /**
         * Constructor.
         *
         * @param query Query map
         */
        public ImageResultsPerPageSetterImpl(CatAPIQueryMap query) {
            this.query = query;
        }

        @Override
        public ImageSizeSetter resultsPerPage(int resultsPerPage) {
            int validResultsPerPage = Constants.CAT_API_DEF_RESULTS_PER_PAGE;
            if (resultsPerPage >= Constants.CAT_API_MIN_RESULTS_PER_PAGE && resultsPerPage <= Constants.CAT_API_MAX_RESULTS_PER_PAGE) {
                validResultsPerPage = resultsPerPage;
            }
            query.addParam("results_per_page", Integer.valueOf(validResultsPerPage).toString());
            return new ImageSizeSetterImpl(query);
        }
    }

    /**
     * Interface for setting the size of the image(s).
     */
    public static interface ImageSizeSetter {
        /**
         * Sets the size of the image(s).
         *
         * @param imageSize CatAPI compatible image size
         * @return Final query builder.
         */
        QueryBuilder size(CatAPIImageSize imageSize);
    }

    /**
     * Implementation for setting the size of the image(s).
     */
    private static final class ImageSizeSetterImpl implements ImageSizeSetter {

        private final CatAPIQueryMap query;

        /**
         * Constructor.
         *
         * @param query Query map.
         */
        public ImageSizeSetterImpl(CatAPIQueryMap query) {
            this.query = query;
        }

        @Override
        public QueryBuilder size(CatAPIImageSize imageSize) {
            query.addParam("size", imageSize.name());
            return new QueryBuilderImpl(query);
        }
    }

    /**
     * Interface for finally building the query map.
     */
    public static interface QueryBuilder {
        /**
         * Builds the query map.
         *
         * @return Query map.
         */
        CatAPIQueryMap build();
    }

    /**
     * Implementation for finally building the query map.
     */
    private static class QueryBuilderImpl implements QueryBuilder {

        private final CatAPIQueryMap query;

        /**
         * Constructor.
         *
         * @param query Query map.
         */
        public QueryBuilderImpl(CatAPIQueryMap query) {
            this.query = query;
        }

        @Override
        public CatAPIQueryMap build() {
            return query;
        }
    }
}
package com.balazscsernai.catpuzzle.network;

/**
 * Interface for query map creator.
 * Created by Balázs on 2015.05.24..
 */
public interface CatAPIQueryMapFactory {

    /**
     * Creates a map for querying medium sized PNG image(s).
     * @param resultsPerPage Number of image(s) per page
     * @return Query map
     */
    CatAPIQueryMap createForHTMLMediumPNG(int resultsPerPage);

    /**
     * Creates a map for querying a specific image.
     * @param id Image ID
     * @return Query map
     */
    CatAPIQueryMap createForSpecific(String id);
}

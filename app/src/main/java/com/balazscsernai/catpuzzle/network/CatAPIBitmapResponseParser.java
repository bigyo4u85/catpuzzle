package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;
import android.util.Log;

import com.balazscsernai.catpuzzle.R;
import com.balazscsernai.catpuzzle.image.BitmapDecoder;

import java.io.IOException;
import java.lang.reflect.Type;

import javax.inject.Inject;

import retrofit.mime.TypedInput;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIBitmapResponseParser implements CatAPIResponseParser<Bitmap> {
    private static final String TAG = CatAPIBitmapResponseParser.class.getSimpleName();

    private final BitmapDecoder bitmapDecoder;

    @Inject
    public CatAPIBitmapResponseParser(BitmapDecoder bitmapDecoder) {
        this.bitmapDecoder = bitmapDecoder;
    }

    @Override
    public Bitmap parse(TypedInput body, Type type) {
        Bitmap bitmap = null;
        try {
            bitmap = bitmapDecoder.decode(body.in(), R.dimen.puzzle_width, R.dimen.puzzle_height);
        } catch (IOException e) {
            Log.e(TAG, "Image decoding failed: " + e.getMessage());
        }
        return bitmap;
    }
}

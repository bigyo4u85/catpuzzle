package com.balazscsernai.catpuzzle.network;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by Balázs on 2015.05.24..
 */
@Singleton
public class CatAPIQueryMapFactoryImpl implements CatAPIQueryMapFactory {

    @Inject
    public CatAPIQueryMapFactoryImpl() {}

    @Override
    public CatAPIQueryMap createForHTMLMediumPNG(int resultsPerPage) {
        return new CatAPIQueryMap.Builder()
                .format(CatAPIImageFormat.HTML)
                .resultsPerPage(resultsPerPage)
                .size(CatAPIImageSize.MED)
                .build();
    }

    @Override
    public CatAPIQueryMap createForSpecific(String id) {
        return new CatAPIQueryMap.Builder()
                .id(id)
                .size(CatAPIImageSize.MED)
                .build();
    }
}

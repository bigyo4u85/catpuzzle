package com.balazscsernai.catpuzzle.network;

import java.lang.reflect.Type;
import java.util.List;

import retrofit.mime.TypedInput;

/**
 * Created by Balázs on 2015.05.24..
 */
public interface CatAPIResponseParser<T> {

    T parse(TypedInput body, Type type);

}

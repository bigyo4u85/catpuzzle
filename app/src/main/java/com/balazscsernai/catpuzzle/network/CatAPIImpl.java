package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.Converter;
import retrofit.http.QueryMap;

/**
 * Created by Balázs on 2015.05.24..
 */
@Singleton
public class CatAPIImpl implements CatAPI {

    private final CatAPI api;

    @Inject
    public CatAPIImpl(Converter converter) {
        api = new RestAdapter.Builder().setEndpoint(CatAPI.API_BASE_PATH).setConverter(converter).build().create(CatAPI.class);
    }

    @Override
    public void getImageList(@QueryMap Map<String, String> queryMap, Callback<CatAPIImages> callback) {
        api.getImageList(queryMap, callback);
    }

    @Override
    public void getImage(@QueryMap Map<String, String> queryMap, Callback<Bitmap> callback) {
        api.getImage(queryMap, callback);
    }

}

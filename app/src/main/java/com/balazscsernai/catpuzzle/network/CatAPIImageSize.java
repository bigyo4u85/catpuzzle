package com.balazscsernai.catpuzzle.network;

/**
 * Created by Balázs on 2015.05.24..
 */
public enum CatAPIImageSize {
    SMALL, MED, FULL;
}

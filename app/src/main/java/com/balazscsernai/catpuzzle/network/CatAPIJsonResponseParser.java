package com.balazscsernai.catpuzzle.network;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.Charset;

import javax.inject.Inject;

import retrofit.mime.TypedInput;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIJsonResponseParser implements CatAPIResponseParser<Object> {

    private static final String TAG = CatAPIJsonResponseParser.class.getSimpleName();

    private final Gson gson;
    private final Charset charset;

    @Inject
    public CatAPIJsonResponseParser(Gson gson) {
        this.gson = gson;
        this.charset = Charset.forName("UTF-8");
    }

    @Override
    public Object parse(TypedInput body, Type type) {
        InputStream is = null;
        Object parsed = null;
        try {
            parsed = gson.fromJson(new InputStreamReader(body.in(), charset), type);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } finally {
            try {
                is.close();
            } catch (IOException ignored) {
                // Ignored
            }
        }
        return parsed;
    }
}

package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;

import retrofit.Callback;

/**
 * Created by Balázs on 2015.05.24..
 */
public interface CatAPIClient {

    void getImageList(int numberOfImages, Callback<CatAPIImages> callback);

    void getImage(String id, Callback<Bitmap> callback);
}

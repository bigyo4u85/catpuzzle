package com.balazscsernai.catpuzzle.network;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIImages implements Serializable {

    private List<CatAPIImage> images;

    public CatAPIImages(){
        images = new LinkedList<CatAPIImage>();
    }

    public void addImage(CatAPIImage image) {
        images.add(image);
    }

    public CatAPIImage getImage(int location) {
        return images.get(location);
    }

    public int size() {
        return images.size();
    }
}

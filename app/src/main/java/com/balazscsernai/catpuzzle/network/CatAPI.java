package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;
import retrofit.http.QueryMap;

/**
 * Created by Balázs on 2015.05.24..
 */
public interface CatAPI {

    public static final String API_BASE_PATH = "http://thecatapi.com/api/images";

    @GET("/get")
    void getImageList(@QueryMap Map<String, String> queryMap, Callback<CatAPIImages> callback);

    @GET("/get")
    void getImage(@QueryMap Map<String, String> queryMap, Callback<Bitmap> callback);
}

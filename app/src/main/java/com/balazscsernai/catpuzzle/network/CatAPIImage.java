package com.balazscsernai.catpuzzle.network;

import java.io.Serializable;

/**
 * Created by Balázs on 2015.05.24..
 */
public class CatAPIImage implements Serializable {

    private String id;
    private String url;

    public String getId() {
        return id;
    }

    public CatAPIImage setId(String id) {
        this.id = id;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public CatAPIImage setUrl(String url) {
        this.url = url;
        return this;
    }
}

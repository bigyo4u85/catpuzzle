package com.balazscsernai.catpuzzle.network;

import android.graphics.Bitmap;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit.Callback;

/**
 * Created by Balázs on 2015.05.24..
 */
@Singleton
public class CatAPIClientImpl implements CatAPIClient {

    private final CatAPI api;
    private final CatAPIQueryMapFactory queryMapFactory;

    @Inject
    public CatAPIClientImpl(CatAPI api, CatAPIQueryMapFactory queryMapFactory) {
        this.api = api;
        this.queryMapFactory = queryMapFactory;
    }

    @Override
    public void getImageList(int numberOfImages, Callback<CatAPIImages> callback) {
        api.getImageList(queryMapFactory.createForHTMLMediumPNG(numberOfImages).getQueryMap(), callback);
    }

    @Override
    public void getImage(String id, Callback<Bitmap> callback) {
        api.getImage(queryMapFactory.createForSpecific(id).getQueryMap(), callback);
    }

}

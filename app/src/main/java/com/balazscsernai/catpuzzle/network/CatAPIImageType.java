package com.balazscsernai.catpuzzle.network;

/**
 * Created by Balázs on 2015.05.24..
 */
public enum CatAPIImageType {
    JPG, PNG, GIF;
}

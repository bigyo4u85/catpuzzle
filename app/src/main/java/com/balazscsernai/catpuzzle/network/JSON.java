package com.balazscsernai.catpuzzle.network;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by Balázs on 2015.05.24..
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface JSON {
}
